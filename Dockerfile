# Set the base image for subsequent instructions
FROM gitlab/gitlab-runner

COPY boot.sh /service_boot
RUN chmod +x /service_boot

# CMD ["/bin/sh", "/service_boot"]