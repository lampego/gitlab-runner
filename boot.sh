#!/usr/bin/env bash

echo "Register configuration..."

gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "JPfoqCxVLBLsxJxzrRFj" \
    --executor "docker" \
    --docker-image alpine:latest \
    --description "lm" \
    --tag-list "docker,aws" \
    --run-untagged="true" \
    --locked="false" \
    --access-level="not_protected"

gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "Z9C7FVWPCxmeN3BXjsSu" \
    --executor "docker" \
    --docker-image alpine:latest \
    --description "ab" \
    --tag-list "docker,aws" \
    --run-untagged="true" \
    --locked="false" \
    --access-level="not_protected"

echo "Start runner..."

gitlab-runner start